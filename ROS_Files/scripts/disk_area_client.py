#!/usr/bin/env python3

import rospy
from my_msgs.srv import ComputeDiskArea

if __name__ == '__main__':
    rospy.init_node("compute_disk_area_client")
    rospy.loginfo("Compute Disk Area client node created")

    rospy.wait_for_service("/compute_disk_area")

    try:
        compute_area = rospy.ServiceProxy("/compute_disk_area", ComputeDiskArea)
        response = compute_area(3)
        rospy.loginfo("Area is : " + str(response.area))
    except rospy.ServiceException as e:
        rospy.logwarn("Service failed: " + str(e))
