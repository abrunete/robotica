#!/usr/bin/env python3

import rospy
import math
from my_msgs.srv import ComputeDiskArea

def handle_compute_area(req):
	return math.pi*req.radius*req.radius


if __name__ == '__main__':
	rospy.init_node("compute_disk_area_server")
	rospy.loginfo("Nodo comienza")
	
	rospy.Service("/compute_disk_area",ComputeDiskArea,handle_compute_area)
	rospy.loginfo("Nodo preparado")
	rospy.spin()
