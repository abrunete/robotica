#!/usr/bin/env python3

import rospy
import math
from turtlesim.srv import TeleportRelative

if __name__ == '__main__':
    rospy.init_node("draw_square")

    rospy.wait_for_service("/turtle1/teleport_relative")
    rospy.loginfo("listo")
    try:
        draw = rospy.ServiceProxy("/turtle1/teleport_relative", TeleportRelative)
        response = draw(1.0,0.0)
        rospy.sleep(1)
        response = draw(1.0,math.pi/2)
        rospy.sleep(1)
        response = draw(1.0,math.pi/2)
        rospy.sleep(1)
        response = draw(1.0,math.pi/2)
    except rospy.ServiceException as e:
        rospy.logwarn("Service failed: " + str(e))
