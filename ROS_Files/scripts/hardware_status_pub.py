#!/usr/bin/env python3

import rospy
from my_msgs.msg import HardwareStatus

if __name__ == '__main__':
	rospy.init_node('hardware_status_publisher')
	rospy.loginfo("El nodo hardware_status_publisher ha comenzado")
	
	pub = rospy.Publisher("/abg1/hardware_status", HardwareStatus, queue_size=10)
	
	rate = rospy.Rate(1)

	while not rospy.is_shutdown():
		msg = HardwareStatus()
		msg.temperature = 25
		msg.are_motors_up = True 
		msg.debug_message = "Todo en orden"
		
		pub.publish(msg)
		rate.sleep()





