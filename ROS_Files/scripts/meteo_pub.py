#!/usr/bin/env python3

import rospy
from std_msgs.msg import String

if __name__ == '__main__':

    rospy.init_node('meteo_pub', anonymous=True)

    pub = rospy.Publisher("/temp", String, queue_size=10)

    rate = rospy.Rate(2)

    while not rospy.is_shutdown():
        msg = String()
        msg.data = "Hola, la temperatura es: " + str(25)
        pub.publish(msg)
        rate.sleep()

    rospy.loginfo("Node was stopped")

