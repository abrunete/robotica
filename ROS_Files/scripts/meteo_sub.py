#!/usr/bin/env python3

import rospy
from std_msgs.msg import String

def callback_receive_temp_data(msg):
	rospy.loginfo("Message received : " + msg.data)


if __name__ == '__main__':
	rospy.init_node('meteo_sub')
	
	sub = rospy.Subscriber("/temp", String, callback_receive_temp_data)

	rospy.spin()

