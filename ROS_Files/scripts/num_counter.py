#!/usr/bin/env python3

import rospy
from std_msgs.msg import String
from std_msgs.msg import Int64
from std_srvs.srv import SetBool

cuenta = 0
pub=0


def sub_callback(msg):
	global cuenta
	cuenta += msg.data
	msg = Int64()
	msg.data = cuenta
	pub.publish(msg)
	#rospy.loginfo("Suma: "+ cuenta)

if __name__ == '__main__':
	rospy.init_node('number_counter')
	rospy.loginfo("El nodo number_counter ha comenzado")
	
	sub = rospy.Subscriber("/number", Int64, sub_callback)
	pub = rospy.Publisher("/number_count", Int64, queue_size=10)
	
	rate = rospy.spin()

	rospy.loginfo("El nodo number_counter ha terminado")



