#!/usr/bin/env python3

import rospy
import random
from std_msgs.msg import Int64

if __name__ == '__main__':
	rospy.init_node('number_publisher')
	rospy.loginfo("El nodo number_publisher ha comenzado")
	
	pub = rospy.Publisher("/number", Int64, queue_size=10)
	
	rate = rospy.Rate(1)

	while not rospy.is_shutdown():
		msg = Int64()
		msg.data = random.randrange(10)
		pub.publish(msg)
		rate.sleep()





