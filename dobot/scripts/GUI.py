
# Desarrollo de una interfaz grafica de usuario para el control del Dobot Magician en ROS

# Autor: German Andres Di Fonzo Caturegli

from Tkinter import *
import rospy
from dobot.srv import *
import time

# Se definen las diferentes funciones del programa

# INICIO

def inicio():
	clear()	
	Label(ventana, text="INICIO", font=(20)).pack(pady=10)
	Label(ventana, text="Bienvenido a ROSDOBOT.").pack(pady=10)
	Label(ventana, text="Seleccione la accion que desea realizar:").pack(pady=10)

	Button(ventana, text="Calibrar el robot", width=25, height=2, command=calibrar).pack(pady=10)
	Button(ventana, text="Control manual (modo JOG)", width=25, height=2, command=modoJOG).pack(pady=10)
	Button(ventana, text="Control punto a punto (modo PTP)", width=25, height=2, command=modoPTP).pack(pady=10)
	Button(ventana, text="Ajustes", width=25, height=2, command=ajustes).pack(pady=10)
	Button(ventana, text="Informacion", width=25, height=2, command=info).pack(pady=10)
	Button(ventana, text="Salir", width=25, height=2, command=ventana.quit).pack(pady=10)

# CALIBRACION

def calibrar():
	clear()
	Label(ventana, text="CALIBRACION", font=(20)).pack(pady=10)
	Label(ventana, text="Defina una posicion para la calibracion.").pack(pady=10)

	# Se piden y se almacenan las coordenadas xyz de la calibracion
	Label(ventana, text="Coordenada x:").pack(pady=10)
	x=Entry(ventana)
	x.pack(pady=10)

	Label(ventana, text="Coordenada y:").pack(pady=10)
	y=Entry(ventana)
	y.pack(pady=10)

	Label(ventana, text="Coordenada z:").pack(pady=10)
	z=Entry(ventana)
	z.pack(pady=10)

	Label(ventana, text="Coordenada r:").pack(pady=10)
	r=Entry(ventana)
	r.pack(pady=10)

	Button(ventana, text="Calibrar", width=20, height=2, command=lambda:calibracion(x.get(),y.get(),z.get(), r.get())).pack(pady=10)

	# Boton para la parada de emergencia
	Button(ventana, text="Parada de Emergencia", width=20, height=2, command=paradaEmergencia, background="red").pack(pady=10)

# La siguiente funcion se encarga de calibrar el robot

def calibrarDobot():

	limpiarCola()

	rospy.wait_for_service('/DobotServer/SetHOMECmd')
	try: 
		
		client = rospy.ServiceProxy('/DobotServer/SetHOMECmd', SetHOMECmd)
		resp = client()
		print(resp.result)
		print("Calibrando robot.")
		empezarCola()
	
	except rospy.ServiceException as e:
		print("Llamada al servicio SetHOMECmd fallida: %s"%e)

def calibracion(coord_x, coord_y , coord_z, coord_r):
	
	x = float(coord_x)
	y = float(coord_y)
	z = float(coord_z)
	r = float(coord_r)
	
	if (x>100 and x<300) and (y>-320 and y<320) and (z>-100 and z<150):

		# Se utilizan estos valores para los servicios SetHOME y HOME
		rospy.wait_for_service('/DobotServer/SetHOMEParams')
		try:	
			client = rospy.ServiceProxy('/DobotServer/SetHOMEParams', SetHOMEParams)
			resp = client(x,y,z,r,1)
			print("Pose de calibracion establecida.")

		except rospy.ServiceException as e:
			print("Llamada al servicio SetHOMEParams fallida: %s"%e)

		calibrarDobot()

	else:
		ventanaError()

# CONTROL MANUAL

pose_xyzr = [0.0, 0.0, 0.0, 0.0]
pose_joint = [0.0, 0.0, 0.0, 0.0]

def modoJOG():
	clear()
	Label(ventana, text="CONTROL MANUAL (modo JOG)", font=(20)).grid(row=0, column=0, columnspan=7, padx=10, pady=10)

	# Control de xyzr (coordenadas cartesianas)
	Label(ventana, text="Sistema de coordenadas cartesianas:").grid(row=1, column=0, columnspan=3, padx=10, pady=10)	
	Label(ventana, text="X").grid(row=2, column=0, padx=10, pady=10)
	Label(ventana, text="Y").grid(row=3, column=0, padx=10, pady=10)
	Label(ventana, text="Z").grid(row=4, column=0, padx=10, pady=10)
	Label(ventana, text="R").grid(row=5, column=0, padx=10, pady=10)	

	Button(ventana, text="+X", width=5, height=2, command=lambda:JOG("0", "1")).grid(row=2, column=1, padx=10, pady=10)
	Button(ventana, text="-X", width=5, height=2, command=lambda:JOG("0", "2")).grid(row=2, column=2, padx=10, pady=10)

	Button(ventana, text="+Y", width=5, height=2, command=lambda:JOG("0","3")).grid(row=3, column=1, padx=10, pady=10)
	Button(ventana, text="-Y", width=5, height=2, command=lambda:JOG("0","4")).grid(row=3, column=2, padx=10, pady=10)

	Button(ventana, text="+Z", width=5, height=2, command=lambda:JOG("0","5")).grid(row=4, column=1, padx=10, pady=10)
	Button(ventana, text="-Z", width=5, height=2, command=lambda:JOG("0","6")).grid(row=4, column=2, padx=10, pady=10)

	Button(ventana, text="+R", width=5, height=2, command=lambda:JOG("0","7")).grid(row=5, column=1, padx=10, pady=10)
	Button(ventana, text="-R", width=5, height=2, command=lambda:JOG("0","8")).grid(row=5, column=2, padx=10, pady=10)

	# Control de J1, J2, J3 y J4 (coordenadas articulares)
	Label(ventana, text="Sistema de coordenadas articulares:").grid(row=1, column=4, columnspan=3, padx=10, pady=10)
	Label(ventana, text="J1").grid(row=2, column=4, padx=10, pady=10)
	Label(ventana, text="J2").grid(row=3, column=4, padx=10, pady=10)
	Label(ventana, text="J3").grid(row=4, column=4, padx=10, pady=10)
	Label(ventana, text="J4").grid(row=5, column=4, padx=10, pady=10)

	Button(ventana, text="+J1", width=5, height=2, command=lambda:JOG("1", "1")).grid(row=2, column=5, padx=10, pady=10)
	Button(ventana, text="-J1", width=5, height=2, command=lambda:JOG("1", "2")).grid(row=2, column=6, padx=10, pady=10)

	Button(ventana, text="+J2", width=5, height=2, command=lambda:JOG("1", "3")).grid(row=3, column=5, padx=10, pady=10)
	Button(ventana, text="-J2", width=5, height=2, command=lambda:JOG("1", "4")).grid(row=3, column=6, padx=10, pady=10)

	Button(ventana, text="+J3", width=5, height=2, command=lambda:JOG("1", "5")).grid(row=4, column=5, padx=10, pady=10)
	Button(ventana, text="-J3", width=5, height=2, command=lambda:JOG("1", "6")).grid(row=4, column=6, padx=10, pady=10)

	Button(ventana, text="+J4", width=5, height=2, command=lambda:JOG("1", "7")).grid(row=5, column=5, padx=10, pady=10)
	Button(ventana, text="-J4", width=5, height=2, command=lambda:JOG("1", "8")).grid(row=5, column=6, padx=10, pady=10)

	# Detener el modo JOG
	Button(ventana, text="Detener modo JOG", width=15, height=2, command=lambda:JOG("0", "0")).grid(row=6, column=0, padx=10, pady=10, columnspan=3)

	# Boton para la parada de emergencia
	Button(ventana, text="Parada de Emergencia", width=20, height=2, command=paradaEmergencia, background="red").grid(row=6, column=3, padx=10, pady=10, columnspan=3)
	
	# Controlar pinza
	Label(ventana, text="Controlar pinza:").grid(row=7, column=0, columnspan=2, padx=10, pady=10)

	Button(ventana, text="Desactivar", width=10, height=2, command=lambda:controlPinza(0,0)).grid(row=7, column=2, padx=10, pady=10)
	Button(ventana, text="Abrir pinza", width=10, height=2, command=lambda:controlPinza(1,0)).grid(row=7, column=3, columnspan=2, padx=10, pady=10)
	Button(ventana, text="Cerrar pinza", width=10, height=2, command=lambda:controlPinza(1,1)).grid(row=7, column=5, columnspan=2, padx=10, pady=10)

	# Conocer la pose actual del robot
	Button(ventana, text="Actualizar pose", width=20, height=2, command=obtenerPose).grid(row=8, column=0, columnspan=3, rowspan = 2, padx=10, pady=10)
	
	xyzr_str = [str(round(pose_xyzr[0],1)),str(round(pose_xyzr[1],1)),str(round(pose_xyzr[2],1)),str(round(pose_xyzr[3],1))]
	Label(ventana, text="X: "+xyzr_str[0]).grid(row=8, column=3, padx=10, pady=10)
	Label(ventana, text="Y: "+xyzr_str[1]).grid(row=8, column=4, padx=10, pady=10)
	Label(ventana, text="Z: "+xyzr_str[2]).grid(row=8, column=5, padx=10, pady=10)
	Label(ventana, text="R: "+xyzr_str[3]).grid(row=8, column=6,  padx=10, pady=10)

	joint_str = [str(round(pose_joint[0],1)),str(round(pose_joint[1],1)),str(round(pose_joint[2],1)),str(round(pose_joint[3],1))]
	Label(ventana, text="J1: "+joint_str[0]).grid(row=9, column=3, padx=10, pady=10)
	Label(ventana, text="J2: "+joint_str[1]).grid(row=9, column=4, padx=10, pady=10)
	Label(ventana, text="J3: "+joint_str[2]).grid(row=9, column=5, padx=10, pady=10)
	Label(ventana, text="J4: "+joint_str[3]).grid(row=9, column=6,  padx=10, pady=10)
	

# Funciones que se utilizan en el modo JOG

def JOG(sist_coord, movimiento):
	
	IsJoint = int(sist_coord)
	CMD = int(movimiento)
	
	# Se utilizan estos valores para el servicio SetJOGCmd
	rospy.wait_for_service('/DobotServer/SetJOGCmd')
	try:	
		client = rospy.ServiceProxy('/DobotServer/SetJOGCmd', SetJOGCmd)
		resp = client(IsJoint, CMD)

	except rospy.ServiceException as e:
		print("Llamada al servicio SetJOGCmd fallida: %s"%e)

	# Se ejecuta durante un tiempo determinado
	rospy.sleep(0.5)

	# Se para el robot
	rospy.wait_for_service('/DobotServer/SetJOGCmd')
	try:	
		client2 = rospy.ServiceProxy('/DobotServer/SetJOGCmd', SetJOGCmd)
		resp = client2(0, 0)

	except rospy.ServiceException as e:
		print("Llamada al servicio SetJOGCmd fallida: %s"%e)

def obtenerPose():
	
	# Se obtiene la pose con el Servicio GetPose y se almacena en un vector
	global pose_xyzr
	global pose_joint

	rospy.wait_for_service('/DobotServer/GetPose')
	try:	
		client = rospy.ServiceProxy('/DobotServer/GetPose', GetPose)
		resp = client()
		pose_xyzr = [resp.x, resp.y, resp.z, resp.r]
		pose_joint = [resp.jointAngle[0], resp.jointAngle[1], resp.jointAngle[2], resp.jointAngle[3]]
		print(pose_xyzr)

	except rospy.ServiceException as e:
		print("Llamada al servicio GetPose fallida: %s"%e)	

	modoJOG()

def controlPinza(desactivar,abrir_cerrar):
	rospy.wait_for_service('/DobotServer/SetEndEffectorGripper')
	try:	
		client = rospy.ServiceProxy('/DobotServer/SetEndEffectorGripper', SetEndEffectorGripper)
		resp = client(desactivar, abrir_cerrar,1)

	except rospy.ServiceException as e:
		print("Llamada al servicio SetEndEffectorGripper fallida: %s"%e)
	
# CONTROL PUNTO A PUNTO

PuntosTrayectoria = list()

def modoPTP():
	clear()
	Label(ventana, text="CONTROL PUNTO A PUNTO (modo PTP)", font=(20)).pack(pady=5)
	Label(ventana, text="Introduzca las coordenadas cartesianas del punto deseado.").pack(pady=5)

	# Se piden y se almacenan las coordenadas xyzr
	Label(ventana, text="Coordenada x:").pack(pady=5)
	x=Entry(ventana)
	x.pack(pady=5)

	Label(ventana, text="Coordenada y:").pack(pady=5)
	y=Entry(ventana)
	y.pack(pady=5)

	Label(ventana, text="Coordenada z:").pack(pady=5)
	z=Entry(ventana)
	z.pack(pady=5)

	Label(ventana, text="Coordenada r:").pack(pady=5)
	r=Entry(ventana)
	r.pack(pady=5)

	# Se pide el tipo de movimiento (MOVJ o MOVL)
	Label(ventana, text="Tipo de movimiento:").pack(pady=5)
	radioValue = IntVar()
	Radiobutton(ventana, text='MOVJ', variable=radioValue, value=1).pack(pady=5)
	Radiobutton(ventana, text='MOVL', variable=radioValue, value=2).pack(pady=5)
	
	Button(ventana, text="Elegir otro punto", width=20, height=2, command=lambda:trayectoria(x.get(),y.get(),z.get(),r.get(),radioValue.get(),"false")).pack(pady=5)

	Button(ventana, text="Generar Trayectoria", width=20, height=2, command=lambda:trayectoria(x.get(),y.get(),z.get(),r.get(),radioValue.get(),"true")).pack(pady=5)

	# Boton para la parada de emergencia
	Button(ventana, text="Parada de Emergencia", width=20, height=2, command=paradaEmergencia, background="red").pack(pady=10)

	if len(PuntosTrayectoria) > 0:
		for j in range(0, len(PuntosTrayectoria)):
			if PuntosTrayectoria[j][4] == 1:
				mov="MOVJ"
			else:
				mov="MOVL"
			texto="Punto "+str(j+1)+":"+" ("+str(PuntosTrayectoria[j][0])+","+str(PuntosTrayectoria[j][1])+","+str(PuntosTrayectoria[j][2])+","+str(PuntosTrayectoria[j][3])+") "+mov
			Label(ventana, text=texto).pack(pady=5)

# Funcion que permite ir almacenando los puntos introducidos y generar la trayectoria 

def trayectoria(coord_x, coord_y, coord_z, coord_r, valor, i):

	x = float(coord_x)
	y = float(coord_y)
	z = float(coord_z)
	r = float(coord_r)
	v = int(valor)
	punto = [x,y,z,r,v]

	if (x>100 and x<300) and (y>-320 and y<320) and (z>-100 and z<150):
		global PuntosTrayectoria
		PuntosTrayectoria.append(punto)

		if i == "false":
			modoPTP()

		elif i == "true":
			print(PuntosTrayectoria)
			# Ejecutar el servicio SetPTPCmd con los puntos introducidos
		
			'''
			rospy.wait_for_service('/DobotServer/SetPTPCmd')
			client = rospy.ServiceProxy('/DobotServer/SetPTPCmd', SetPTPCmd)
			i = 0
			resp7 = client(v,x,y,z,r)
			time.sleep(3)
	
			while True:
				print(resp7.result)
				if resp7.result != 0:
                    			resp7 = client(v,x,y,z,r)
			
               			else:
                    			break
			while i<100:
                		resp7 = client(v,x,y,z,r)
                		if resp7.result != 0:
                    			resp = client(v,x,y,z,r)
				print(resp7.result)
               			#if resp7.result == 1:
                    			#break
				i=i+1
		
			'''
			for j in range(0, len(PuntosTrayectoria)):
				print(PuntosTrayectoria[j])
				rospy.wait_for_service('/DobotServer/SetPTPCmd')
				try:	
				
					client = rospy.ServiceProxy('/DobotServer/SetPTPCmd', SetPTPCmd)
					c_x = PuntosTrayectoria[j][0]
					c_y = PuntosTrayectoria[j][1]
					c_z = PuntosTrayectoria[j][2]
					c_r = PuntosTrayectoria[j][3]
					c_v = PuntosTrayectoria[j][4]
					resp = client(c_v,c_x,c_y,c_z,c_r)
					#time.sleep(1)
	
					while resp.result != 0:
						print(resp.result)
                    				resp = client(c_v,c_x,c_y,c_z,c_r)

				except rospy.ServiceException as e:
					print("Llamada al servicio SetPTPCmd fallida: %s"%e)
					break

		
			# Reiniciar el valor de PuntosTrayectoria

			for k in range(0, len(PuntosTrayectoria)):
				PuntosTrayectoria.pop()
			print(PuntosTrayectoria)
		
			modoPTP()

	else:
		ventanaError()

# AJUSTES DE PARAMETROS DE VELOCIDAD Y ACELERACION

# Inicializacion

# Parametros Modo JOG
velocidadJ_JOG = [50,50,50,50]
aceleracionJ_JOG = [50,50,50,50]
velocidadXYZ_JOG = [50,50,50,50]
aceleracionXYZ_JOG = [50,50,50,50]
ratioV_JOG = 50
ratioA_JOG = 50

# Parametros modo PTP
velocidadXYZ_PTP = 50
velocidadR_PTP = 50
aceleracionXYZ_PTP = 50
aceleracionR_PTP = 50
ratioV_PTP = 50
ratioA_PTP = 50

# Funciones para modificar ajustes

def ajustes():
	clear()
	Label(ventana, text="AJUSTES", font=(20)).pack(pady=10)
	
	Button(ventana, text="Ajustes modo JOG", width=25, height=2, command=ajustesJOG).pack(pady=10)
	Button(ventana, text="Ajustes modo PTP", width=25, height=2, command=ajustesPTP).pack(pady=10)

def ajustesJOG():
	clear()
	Label(ventana, text="AJUSTES MODO JOG", font=(20)).grid(row=0, column=0, columnspan=4, padx=10, pady=10)

	# Velocidad y aceleracion de articulaciones

	Label(ventana, text="Velocidad de J1/J2/J3/J4 (grados/s):").grid(row=1, column=0, padx=10, pady=10)
	v_J=Entry(ventana)
	v_J.grid(row=1, column=1, padx=10, pady=10)
	val_actual = "("+str(velocidadJ_JOG[0])+" grados/s)"
	Label(ventana, text=val_actual).grid(row=1, column=2, padx=10, pady=10)

	Label(ventana, text="Aceleracion de J1/J2/J3/J4 (grados/s2):").grid(row=2, column=0, padx=10, pady=10)
	a_J=Entry(ventana)
	a_J.grid(row=2, column=1, padx=10, pady=10)
	val_actual = "("+str(aceleracionJ_JOG[0])+" grados/s2)"
	Label(ventana, text=val_actual).grid(row=2, column=2, padx=10, pady=10)
	
	# Velocidad y aceleracion en xyz

	Label(ventana, text="Velocidad de X/Y/Z/R (mm/s):").grid(row=3, column=0, padx=10, pady=10)
	v_XYZ=Entry(ventana)
	v_XYZ.grid(row=3, column=1, padx=10, pady=10)
	val_actual = "("+str(velocidadXYZ_JOG[0])+" mm/s)"
	Label(ventana, text=val_actual).grid(row=3, column=2, padx=10, pady=10)

	Label(ventana, text="Aceleracion de X/Y/Z/R (mm/s2):").grid(row=4, column=0, padx=10, pady=10)
	a_XYZ=Entry(ventana)
	a_XYZ.grid(row=4, column=1, padx=10, pady=10)
	val_actual = "("+str(aceleracionXYZ_JOG[0])+" mm/s2)"
	Label(ventana, text=val_actual).grid(row=4, column=2, padx=10, pady=10)

	# Ratios

	Label(ventana, text="Ratio de velocidad (%):").grid(row=5, column=0, padx=10, pady=10)
	ratioV=Entry(ventana)
	ratioV.grid(row=5, column=1, padx=10, pady=10)
	val_actual = "("+str(ratioV_JOG)+"%)"
	Label(ventana, text=val_actual).grid(row=5, column=2, padx=10, pady=10)

	Label(ventana, text="Ratio de aceleracion (%):").grid(row=6, column=0, padx=10, pady=10)
	ratioA=Entry(ventana)
	ratioA.grid(row=6, column=1, padx=10, pady=10)
	val_actual = "("+str(ratioA_JOG)+"%)"
	Label(ventana, text=val_actual).grid(row=6, column=2, padx=10, pady=10)

	Button(ventana, text="Guardar", width=15, height=2, command=lambda:SetParametrosJOG(v_J.get(),a_J.get(),v_XYZ.get(),a_XYZ.get(),ratioV.get(),ratioA.get())).grid(row=7, column=0, columnspan=4, padx=10, pady=10)


def SetParametrosJOG(vJ,aJ,vXYZ,aXYZ,rV,rA):
	
	global velocidadJ_JOG
	global aceleracionJ_JOG
	global velocidadXYZ_JOG
	global aceleracionXYZ_JOG
	global ratioV_JOG
	global ratioA_JOG

	velocidadJ_JOG = [float(vJ),float(vJ),float(vJ),float(vJ)]
	aceleracionJ_JOG = [float(aJ),float(aJ),float(aJ),float(aJ)]
	velocidadXYZ_JOG = [float(vXYZ),float(vXYZ),float(vXYZ),float(vXYZ)]
	aceleracionXYZ_JOG = [float(aXYZ),float(aXYZ),float(aXYZ),float(aXYZ)]
	ratioV_JOG = float(rV)
	ratioA_JOG = float(rA)

	# Velocidades (grados/s) y aceleraciones (grados/s al cuadrado) angulares de las articulaciones del modo JOG
	setVelAcelJointJOG(velocidadJ_JOG, aceleracionJ_JOG,1)

	# Velocidades (mm/s) y aceleraciones (mm/s al cuadrado) lineales de los ejes cartesianos del modo JOG
	setVelAcelCartJOG(velocidadXYZ_JOG, aceleracionXYZ_JOG,1)

	# Ratios de velocidad y aceleracion para modo JOG
	setRatiosVelAcelJOG(ratioV_JOG,ratioA_JOG,1)

	ajustesJOG()
	
def ajustesPTP():
	clear()
	Label(ventana, text="AJUSTES MODO PTP", font=(20)).grid(row=0, column=0, columnspan=4, padx=10, pady=10)

	Label(ventana, text="Velocidad xyz (mm/s):").grid(row=1, column=0, padx=10, pady=10)
	v_xyz=Entry(ventana)
	v_xyz.grid(row=1, column=1, padx=10, pady=10)
	val_actual = "("+str(velocidadXYZ_PTP)+" mm/s)"
	Label(ventana, text=val_actual).grid(row=1, column=2, padx=10, pady=10)
	
	Label(ventana, text="Velocidad r (mm/s):").grid(row=2, column=0, padx=10, pady=10)
	v_r=Entry(ventana)
	v_r.grid(row=2, column=1, padx=10, pady=10)
	val_actual = "("+str(velocidadR_PTP)+" mm/s)"
	Label(ventana, text=val_actual).grid(row=2, column=2, padx=10, pady=10)
	
	Label(ventana, text="Aceleracion xyz (mm/s2):").grid(row=3, column=0, padx=10, pady=10)
	a_xyz=Entry(ventana)
	a_xyz.grid(row=3, column=1, padx=10, pady=10)
	val_actual = "("+str(aceleracionXYZ_PTP)+" mm/s2)"
	Label(ventana, text=val_actual).grid(row=3, column=2, padx=10, pady=10)
	
	Label(ventana, text="Aceleracion r (mm/s2):").grid(row=4, column=0, padx=10, pady=10)
	a_r=Entry(ventana)
	a_r.grid(row=4, column=1, padx=10, pady=10)
	val_actual = "("+str(aceleracionR_PTP)+" mm/s2)"
	Label(ventana, text=val_actual).grid(row=4, column=2, padx=10, pady=10)

	Label(ventana, text="Ratio velocidad (%):").grid(row=5, column=0, padx=10, pady=10)
	ratioV=Entry(ventana)
	ratioV.grid(row=5, column=1, padx=10, pady=10)
	val_actual = "("+str(ratioV_PTP)+"%)"
	Label(ventana, text=val_actual).grid(row=5, column=2, padx=10, pady=10)

	Label(ventana, text="Ratio aceleracion (%):").grid(row=6, column=0, padx=10, pady=10)
	ratioA=Entry(ventana)
	ratioA.grid(row=6, column=1, padx=10, pady=10)
	val_actual = "("+str(ratioA_PTP)+"%)"
	Label(ventana, text=val_actual).grid(row=6, column=2, padx=10, pady=10)

	Button(ventana, text="Guardar", width=15, height=2, command=lambda:SetParametrosPTP(v_xyz.get(),v_r.get(),a_xyz.get(),a_r.get(),ratioV.get(),ratioA.get())).grid(row=7, column=0, columnspan=4, padx=10, pady=10) 	

def SetParametrosPTP(vXYZ, vR, aXYZ, aR, rV, rA):

	global velocidadXYZ_PTP 
	global velocidadR_PTP
	global aceleracionXYZ_PTP
	global aceleracionR_PTP
	global ratioV_PTP
	global ratioA_PTP

	velocidadXYZ_PTP = float(vXYZ)
	velocidadR_PTP = float(vR)
	aceleracionXYZ_PTP = float(aXYZ)
	aceleracionR_PTP = float(aR)
	ratioV_PTP = float(rV)
	ratioA_PTP = float(rA)

	# Velocidades (mm/s) y aceleraciones (mm/s al cuadrado) lineales de los ejes cartesianos del modo PTP
	setVelAcelCartPTP(velocidadXYZ_PTP,velocidadR_PTP,aceleracionXYZ_PTP,aceleracionR_PTP,1)

	# Ratios de velocidad y aceleracion para modo PTP
	setRatiosVelAcelPTP(ratioV_PTP,ratioA_PTP,1)

	ajustesPTP()

# INFORMACION ACERCA DE LA GUI

def info():
	clear()
	Label(ventana, text="INFORMACION", font=(20)).pack(pady=10)
	Label(ventana, text="En este apartado se ofrece informacion importante acerca del funcionamiento de la GUI.").pack(pady=10)
	Label(ventana, text="--------------------------").pack(pady=0)
	Label(ventana, text="- Para calibrar el robot es importante introducir siempre las coordenadas").pack(pady=10)
	Label(ventana, text="cartesianas del punto de calibracion. De lo contrario, no se calibrara.").pack(pady=10)
	Label(ventana, text="--------------------------").pack(pady=0)
	Label(ventana, text="- En el control PTP, para poder elegir otro punto o generar la trayectoria es").pack(pady=10)
	Label(ventana, text="necesario que los campos que se piden (coordenadas y tipo de movimiento) esten").pack(pady=10)
	Label(ventana, text="correctamente rellenados a la hora de presionar los botones. De lo contrario").pack(pady=10)
	Label(ventana, text="la interfaz no obedecera.").pack(pady=10)
	Label(ventana, text="--------------------------").pack(pady=0)
	Label(ventana, text="- Tanto las coordenadas introducidas en la calibracion o en el modo PTP deben").pack(pady=10)
	Label(ventana, text="estar dentro del espacio de trabajo del Dobot. Sino, aparecera una ventana de").pack(pady=10)
	Label(ventana, text="advertencia para que se cambien los valores introducidos.").pack(pady=10)
	Label(ventana, text="--------------------------").pack(pady=0)
	Label(ventana, text="- Para poder guardar los nuevos parametros de velocidad y aceleracion en los menus de").pack(pady=10)
	Label(ventana, text="ajustes, es necesario que todos los campos esten rellenados al presionar el boton guardar.").pack(pady=10)
	Label(ventana, text="--------------------------").pack(pady=0)
	Label(ventana, text="- Despues de presionar la parada de emergencia, se recomienda calibrar de nuevo el robot.").pack(pady=10)

# MENU SUPERIOR

def menuSuperior():
	menu_superior = Menu(ventana)
	menu_superior.add_command(label="Inicio", command=inicio)
	menu_superior.add_command(label="Calibracion", command=calibrar)
	menu_superior.add_command(label="Modo JOG", command=modoJOG)
	menu_superior.add_command(label="Modo PTP", command=modoPTP)
	menu_superior.add_command(label="Ajustes", command=ajustes)
	menu_superior.add_command(label="Informacion", command=info)
	menu_superior.add_command(label="Salir", command=ventana.quit)
	ventana.config(menu=menu_superior)

# Fucion para abrir una ventana de advertencia en caso de introducir mal las coordenadas

def ventanaError():
	vent_error = Tk()
	vent_error.geometry("700x50")
	vent_error.title("ADVERTENCIA")
	vent_error.resizable(0,0)
	Label(vent_error,text="CUIDADO! Los coordenadas introducidas pueden estar fuera del espacio de trabajo del robot.").pack(pady=10)
	vent_error.mainloop()

# Funcion para borrar los widgets de la ventana

def clear():
	lista_pack = ventana.pack_slaves()
	for l in lista_pack:
		l.destroy()
	lista_grid = ventana.grid_slaves()
	for l in lista_grid:
		l.destroy()

# Funcion para detener el robot inmediatamente

def paradaEmergencia():
	rospy.wait_for_service('/DobotServer/SetQueuedCmdForceStopExec')
	try:	
		client = rospy.ServiceProxy('/DobotServer/SetQueuedCmdForceStopExec', SetQueuedCmdForceStopExec)
		resp = client()
		print("Parada de Emergencia presionada")

	except rospy.ServiceException as e:
		print("Llamada al servicio SetQueuedCmdForceStopExec fallida: %s"%e)

# Funcion para establecer el timeout

def setTimeout(time):
	rospy.wait_for_service('/DobotServer/SetCmdTimeout')
	try:	
		client = rospy.ServiceProxy('/DobotServer/SetCmdTimeout', SetCmdTimeout)
		resp = client(time)

	except rospy.ServiceException as e:
		print("Llamada al servicio SetCmdTimeout fallida: %s"%e)

# Funcion para limpiar la cola de comandos

def limpiarCola():
	rospy.wait_for_service('/DobotServer/SetQueuedCmdClear')
	try:	
		client = rospy.ServiceProxy('/DobotServer/SetQueuedCmdClear', SetQueuedCmdClear)
		resp = client()

	except rospy.ServiceException as e:
		print("Llamada al servicio SetQueuedCmdClear fallida: %s"%e)

# Funcion que permite empezar a ejecutar comandos de la cola

def empezarCola():
	rospy.wait_for_service('/DobotServer/SetQueuedCmdStartExec')
	try:	
		client = rospy.ServiceProxy('/DobotServer/SetQueuedCmdStartExec', SetQueuedCmdStartExec)
		resp = client()

	except rospy.ServiceException as e:
		print("Llamada al servicio SetQueuedCmdStartExec fallida: %s"%e)

# Funcion para obtener la informacion de la version del dispositivo

def getVersionInfo():
	rospy.wait_for_service('/DobotServer/GetDeviceVersion')
	try:	
		client = rospy.ServiceProxy('/DobotServer/GetDeviceVersion', GetDeviceVersion)
		resp = client()
		print("Version del dispositivo: "+str(resp.majorVersion)+"."+str(resp.minorVersion)+"."+str(resp.revision))

	except rospy.ServiceException as e:
		print("Llamada al servicio GetDeviceVersion fallida: %s"%e)

# Funcion para establecer offset del efector final

def establecerOffset(x_bias,y_bias,z_bias):
	rospy.wait_for_service('/DobotServer/SetEndEffectorParams')
	try:	
		client = rospy.ServiceProxy('/DobotServer/SetEndEffectorParams', SetEndEffectorParams)
		resp = client(x_bias,y_bias,z_bias,1)

	except rospy.ServiceException as e:
		print("Llamada al servicio SetEndEffectorParams fallida: %s"%e)

# Funciones para establecer la velocidad y aceleracion en modo PTP

def setVelAcelJointPTP(vel,acel,introducirCola):
	rospy.wait_for_service('/DobotServer/SetPTPJointParams')
	try:	
		client = rospy.ServiceProxy('/DobotServer/SetPTPJointParams', SetPTPJointParams)
		resp = client(vel,acel,introducirCola)

	except rospy.ServiceException as e:
		print("Llamada al servicio SetPTPJointParams fallida: %s"%e)

def setVelAcelCartPTP(xyzVel, rVel, xyzAcel, rAcel,introducirCola):
	rospy.wait_for_service('/DobotServer/SetPTPCoordinateParams')
	try:	
		client = rospy.ServiceProxy('/DobotServer/SetPTPCoordinateParams', SetPTPCoordinateParams)
		resp = client(xyzVel, rVel, xyzAcel, rAcel,introducirCola)

	except rospy.ServiceException as e:
		print("Llamada al servicio SetPTPCoordinateParams fallida: %s"%e)

def setRatiosVelAcelPTP(ratioVel,ratioAcel,introducirCola):
	rospy.wait_for_service('/DobotServer/SetPTPCommonParams')
	try:	
		client = rospy.ServiceProxy('/DobotServer/SetPTPCommonParams', SetPTPCommonParams)
		resp = client(ratioVel,ratioAcel,introducirCola)

	except rospy.ServiceException as e:
		print("Llamada al servicio SetPTPCommonParams fallida: %s"%e)

# Funciones para establecer la velocidad y aceleracion en modo JOG

def setVelAcelJointJOG(vel, acel,introducirCola):
	rospy.wait_for_service('/DobotServer/SetJOGJointParams')
	try:	
		client = rospy.ServiceProxy('/DobotServer/SetJOGJointParams', SetJOGJointParams)
		resp = client(vel,acel,introducirCola)

	except rospy.ServiceException as e:
		print("Llamada al servicio SetJOGJointParams fallida: %s"%e)

def setVelAcelCartJOG(vel, acel,introducirCola):
	rospy.wait_for_service('/DobotServer/SetJOGCoordinateParams')
	try:	
		client = rospy.ServiceProxy('/DobotServer/SetJOGCoordinateParams', SetJOGCoordinateParams)
		resp = client(vel, acel,introducirCola)

	except rospy.ServiceException as e:
		print("Llamada al servicio SetJOGCoordinateParams fallida: %s"%e)

def setRatiosVelAcelJOG(ratioVel,ratioAcel,introducirCola):
	rospy.wait_for_service('/DobotServer/SetJOGCommonParams')
	try:	
		client = rospy.ServiceProxy('/DobotServer/SetJOGCommonParams', SetJOGCommonParams)
		resp = client(ratioVel,ratioAcel,introducirCola)

	except rospy.ServiceException as e:
		print("Llamada al servicio SetJOGCommonParams fallida: %s"%e)
# FUNCION MAIN

# Se define la ventana de la GUI
ventana = Tk()
ventana.geometry("600x700")
ventana.title("ROSDOBOT")
ventana.resizable(0,0)
ventana.configure(background="CadetBlue")

# Se establece el timeout en ms
setTimeout(3000)

# Se limpia la cola de mensajes
limpiarCola()

# Empezar a ejecutar comandos que llegan a la cola
empezarCola()

# Obtener informacion de la version del dispositivo
getVersionInfo()

# Se establece el ofsset del efector final: la pinza
establecerOffset(30.0,0.0,80.0)

# Velocidades (grados/s) y aceleraciones (grado/s al cuadrado) angulares de las articulaciones del modo PTP
setVelAcelJointPTP([50,50,50,50], [50,50,50,50], 1)

# Velocidades (mm/s) y aceleraciones (mm/s al cuadrado) lineales de los ejes cartesianos del modo PTP
setVelAcelCartPTP(50,50,50,50,1)

# Ratios de velocidad y aceleracion para modo PTP
setRatiosVelAcelPTP(50,50,1)

# Velocidades (grados/s) y aceleraciones (grados/s al cuadrado) angulares de las articulaciones del modo JOG
setVelAcelJointJOG([50,50,50,50], [50,50,50,50],1)

# Velocidades (mm/s) y aceleraciones (mm/s al cuadrado) lineales de los ejes cartesianos del modo JOG
setVelAcelCartJOG([50,50,50,50], [50,50,50,50],1)

# Ratios de velocidad y aceleracion para modo JOG
setRatiosVelAcelJOG(50,50,1)

# Se coloca el menu en la parte superior y se abre la ventana de INICIO
menuSuperior()
inicio()

# Se carga la ventana
ventana.mainloop()
