
/* Programa desarrolado y comentado por Germán Andrés Di Fonzo Caturegli */

/* Diseño y desarrollo de un nodo cliente ROS para que el robot Dobot Magician agarre un objeto situado en una posición determinada y lo suelte en otra distinta */

/* Librerías */

#include "ros/ros.h"
#include "ros/console.h"
#include "std_msgs/String.h"
#include "dobot/SetCmdTimeout.h"
#include "dobot/SetQueuedCmdClear.h"
#include "dobot/SetQueuedCmdStartExec.h"
#include "dobot/SetQueuedCmdForceStopExec.h"
#include "dobot/GetDeviceVersion.h"

#include "dobot/SetEndEffectorParams.h"
#include "dobot/SetPTPJointParams.h"
#include "dobot/SetPTPCoordinateParams.h"
#include "dobot/SetPTPJumpParams.h"
#include "dobot/SetPTPCommonParams.h"
#include "dobot/SetHOMEParams.h"
#include "dobot/SetHOMECmd.h"
#include "dobot/SetPTPCmd.h"
#include "dobot/SetEndEffectorGripper.h"
#include "dobot/SetWAITCmd.h"

/* Función principal */

int main(int argc, char **argv)
{

    /* Creación e inicialización de un nodo denominado "DobotClient" */
    
    ros::init(argc, argv, "DobotClient");
    ros::NodeHandle n;

    /* Declaración del cliente que realizará las peticiones a los servicios */

    ros::ServiceClient client;

    /* Se establece el timeout en 3 segundos (3000 milisegundos) */

    client = n.serviceClient<dobot::SetCmdTimeout>("/DobotServer/SetCmdTimeout");
    dobot::SetCmdTimeout srv1;
    srv1.request.timeout = 3000;
    if (client.call(srv1) == false) {
        ROS_ERROR("Error al llamar al servicio SetCmdTimeout. ¡Quizás el nodo DobotServer no se ha lanzado todavia!");
        return -1;
    }

    /* Se limpia la cola de comandos */

    client = n.serviceClient<dobot::SetQueuedCmdClear>("/DobotServer/SetQueuedCmdClear");
    dobot::SetQueuedCmdClear srv2;
    client.call(srv2);

    /* Se empiezan a ejecutar los comandos que se van introduciendo en la cola siguiendo un orden */

    client = n.serviceClient<dobot::SetQueuedCmdStartExec>("/DobotServer/SetQueuedCmdStartExec");
    dobot::SetQueuedCmdStartExec srv3;
    client.call(srv3);
   
    /* Se obtiene la información acerca de la versión del dispositivo */

    client = n.serviceClient<dobot::GetDeviceVersion>("/DobotServer/GetDeviceVersion");
    dobot::GetDeviceVersion srv4;
    client.call(srv4);
    if (srv4.response.result == 0) {
        ROS_INFO("Version del dispositivo:%d.%d.%d", srv4.response.majorVersion, srv4.response.minorVersion, srv4.response.revision);
    } else {
        ROS_ERROR("¡Fallo en la obtención de la informacion de la version!");
    }

    /* Se establece el offset del efector final (pinza) */

    client = n.serviceClient<dobot::SetEndEffectorParams>("/DobotServer/SetEndEffectorParams");
    dobot::SetEndEffectorParams srv5;
    srv5.request.xBias = 70;
    srv5.request.yBias = 0;
    srv5.request.zBias = 0;
    client.call(srv5);

    /* Se establece la velocidad y aceleración angulares del eje de coordenadas articulares para el modo PTP */

    do {
        client = n.serviceClient<dobot::SetPTPJointParams>("/DobotServer/SetPTPJointParams");
        dobot::SetPTPJointParams srv;

        for (int i = 0; i < 4; i++) {
            srv.request.velocity.push_back(100);     // º/s
        }
        for (int i = 0; i < 4; i++) {
            srv.request.acceleration.push_back(100); // º/s²
        }
        client.call(srv);
    } while (0);

    /* Se establece la velocidad y aceleracion lineales del eje de coordenadas cartesiananas para el modo PTP */

    do {
        client = n.serviceClient<dobot::SetPTPCoordinateParams>("/DobotServer/SetPTPCoordinateParams");
        dobot::SetPTPCoordinateParams srv;

        srv.request.xyzVelocity = 100;      // mm/s
        srv.request.xyzAcceleration = 100;  // mm/s²
        srv.request.rVelocity = 100;        // mm/s
        srv.request.rAcceleration = 100;    // mm/s²
        client.call(srv);
    } while (0);

    /* Se establecen los ratios de velocidad y aceleración para el modo PTP */

    do {
        client = n.serviceClient<dobot::SetPTPCommonParams>("/DobotServer/SetPTPCommonParams");
        dobot::SetPTPCommonParams srv;

        srv.request.velocityRatio = 50;
        srv.request.accelerationRatio = 50;
        client.call(srv);
    } while (0);

    /* Se establece la posición de calibración */

    do{
        client = n.serviceClient<dobot::SetHOMEParams>("/DobotServer/SetHOMEParams");  
        dobot::SetHOMEParams home;

        home.request.x = 220; 
        home.request.y = 0; 
        home.request.z = 120; 
        home.request.r = 12.0; 
        home.request.isQueued = 1; 
        client.call(home);
 
    } while(0);

    /* Se calibra el robot antes de empezar con la tarea */

    do{
        client = n.serviceClient<dobot::SetHOMECmd>("/DobotServer/SetHOMECmd"); // SetHOMECmd necesita ser llamado después de SetHOMEParams
        dobot::SetHOMECmd home1;
        client.call(home1); 
    } while(0);

    ros::spinOnce(); // Todas las callbacks del servidor recibidas hasta ahora se ejecutan con spinOnce() 
  

    /* Movimiento del dobot magician empleando el modo PTP (punto a punto) */

    client = n.serviceClient<dobot::SetPTPCmd>("/DobotServer/SetPTPCmd");
    dobot::SetPTPCmd PTP1;

        /* Primer punto */

        do {
            PTP1.request.ptpMode = 1; // modo MOVJ
            PTP1.request.x = 220;
            PTP1.request.y = -160;
            PTP1.request.z = 140;
            PTP1.request.r = 12.0;
            client.call(PTP1); 
            if (PTP1.response.result == 0) { // Este if se ejecuta en caso de no haber error, es decir, se sale del bucle while
                break;
            }     
            ros::spinOnce();
            if (ros::ok() == false) {
                break;
            }
        } while (1);

	ros::spinOnce();

	/* Programación de la pinza para agarrar el objeto */

	client = n.serviceClient<dobot::SetEndEffectorGripper>("/DobotServer/SetEndEffectorGripper");
        dobot::SetEndEffectorGripper grp1;
   
    	do{
           grp1.request.enableCtrl = 1; // Cuando enableCtrl == 1 el motor que abre o cierra la pinza se puede controlar.
           grp1.request.grip = 0; // Cuando grip == 0 la pinza se abre (para agarrar el objeto). 
           grp1.request.isQueued = true; // Este comando carga la petición en la cola.
           client.call(grp1);
    	} while(0);	

	ros::spinOnce();

	client = n.serviceClient<dobot::SetPTPCmd>("/DobotServer/SetPTPCmd");
        dobot::SetPTPCmd PTP2;
        /* Segundo punto */

        do {
            PTP2.request.ptpMode = 2; // modo MOVL
            PTP2.request.x = 220;
            PTP2.request.y = -160;
            PTP2.request.z = -12;
            PTP2.request.r = 12;
            client.call(PTP2); 
            if (PTP2.response.result == 0) { // Este if se ejecuta en caso de no haber error, es decir, se sale del bucle while
                break;
            }
            ros::spinOnce(); 
            if (ros::ok() == false) {
                break;
            }
        } while (1);
	
	ros::spinOnce();
	
	/*Espera de 1 segundo para coger el objeto*/

	client = n.serviceClient<dobot::SetWAITCmd>("/DobotServer/SetWAITCmd");
        dobot::SetWAITCmd srv8;
	srv8.request.timeout = 500;
	client.call(srv8);

	ros::spinOnce();

	/* Programación de la pinza para agarrar el objeto */

	client = n.serviceClient<dobot::SetEndEffectorGripper>("/DobotServer/SetEndEffectorGripper");
        dobot::SetEndEffectorGripper grp2;
   
    	do{
           grp2.request.enableCtrl = 1; // Cuando enableCtrl == 1 el motor que abre o cierra la pinza se puede controlar.
           grp2.request.grip = 1; // Cuando grip == 1 la pinza se cierra (agarra el objeto). 
           grp2.request.isQueued = true; // Este comando carga la petición en la cola.
           client.call(grp2);
    	} while(0);
	
	ros::spinOnce();

	/*Espera de 1 segundo para coger el objeto*/

	client = n.serviceClient<dobot::SetWAITCmd>("/DobotServer/SetWAITCmd");
        dobot::SetWAITCmd srv6;
	srv6.request.timeout = 1000;
	client.call(srv6);

	ros::spinOnce();

	client = n.serviceClient<dobot::SetPTPCmd>("/DobotServer/SetPTPCmd");
    	dobot::SetPTPCmd PTP3;

        /* Se vuelve al primer punto */

        do {
            PTP3.request.ptpMode = 1; // modo MOVJ
            PTP3.request.x = 220;
            PTP3.request.y = -160;
            PTP3.request.z = 140;
            PTP3.request.r = 12;
            client.call(PTP3); 
            if (PTP3.response.result == 0) { // Este if se ejecuta en caso de no haber error, es decir, se sale del bucle while
                break;
            }
            ros::spinOnce(); 
            if (ros::ok() == false) {
                break;
            }
        } while (1);

	ros::spinOnce();

	client = n.serviceClient<dobot::SetPTPCmd>("/DobotServer/SetPTPCmd");
    	dobot::SetPTPCmd PTP4;

        /* Tercer punto */

        do {
            PTP4.request.ptpMode = 1; // modo MOVJ
            PTP4.request.x = 220;
            PTP4.request.y = 160;
            PTP4.request.z = 140;
            PTP4.request.r = 0;
            client.call(PTP4); 
            if (PTP4.response.result == 0) { // Este if se ejecuta en caso de no haber error, es decir, se sale del bucle while
                break;
            }
            ros::spinOnce(); 
            if (ros::ok() == false) {
                break;
            }
        } while (1);

	ros::spinOnce();

        /* Cuarto punto */

        do {
            PTP4.request.ptpMode = 2; // modo MOVL
            PTP4.request.x = 220;
            PTP4.request.y = 160;
            PTP4.request.z = -12;
            PTP4.request.r = 12;
            client.call(PTP4); 
            if (PTP4.response.result == 0) { // Este if se ejecuta en caso de no haber error, es decir, se sale del bucle while
                break;
            }
            ros::spinOnce(); 
            if (ros::ok() == false) {
                break;
            }
        } while (1); 

	ros::spinOnce();

	/*Espera de 1 segundo*/

	client = n.serviceClient<dobot::SetWAITCmd>("/DobotServer/SetWAITCmd");
        dobot::SetWAITCmd srv7;
	srv7.request.timeout = 1000;
	client.call(srv7);

	ros::spinOnce();

	/* Programación de la pinza para soltar el objeto */

	client = n.serviceClient<dobot::SetEndEffectorGripper>("/DobotServer/SetEndEffectorGripper");
        dobot::SetEndEffectorGripper grp3;
   
    	do{
           grp3.request.enableCtrl = 1; // Cuando enableCtrl == 1 el motor que abre o cierra la pinza se enciende.
           grp3.request.grip = 0; // Cuando grip == 0 la pinza se abre (suelta el objeto). 
           grp3.request.isQueued = true; // Este comando carga la petición en la cola.
           client.call(grp3);
    	} while(0);

	ros::spinOnce();
 
	client = n.serviceClient<dobot::SetPTPCmd>("/DobotServer/SetPTPCmd");
    	dobot::SetPTPCmd PTP5;

        /* Se vuelve al tercer punto */

        do {
            PTP5.request.ptpMode = 1; // modo MOVJ
            PTP5.request.x = 220;
            PTP5.request.y = 160;
            PTP5.request.z = 140;
            PTP5.request.r = 12;
            client.call(PTP5); 
            if (PTP5.response.result == 0) { // Este if se ejecuta en caso de no haber error, es decir, se sale del bucle while
                break;
            }
            ros::spinOnce(); 
            if (ros::ok() == false) {
                break;
            }
        } while (1);

	ros::spinOnce();

	/*Espera de 1 segundo*/

	client = n.serviceClient<dobot::SetWAITCmd>("/DobotServer/SetWAITCmd");
        dobot::SetWAITCmd srv9;
	srv9.request.timeout = 1000;
	client.call(srv9);

	ros::spinOnce();

	/* Se desactiva el control de la pinza (se apaga el motor) */

	client = n.serviceClient<dobot::SetEndEffectorGripper>("/DobotServer/SetEndEffectorGripper");
    	dobot::SetEndEffectorGripper grp4;
    
    	do{    
           grp4.request.grip = 0;
           grp4.request.enableCtrl = 0; 
           grp4.request.isQueued = true;
           client.call(grp4);
    	} while(0);

     

        ros::spinOnce(); 



    return 0;
}
















